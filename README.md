# RETROWAVE PSC ERIS Theme (PlayStation Classic)

**By mykehdoom**

```
PLEASE NOTE: This theme is user submitted and not endorsed or created by ModMyClassic. All material should be free to use with third party agreement to allow the use of any third party materials. If you believe there is a conflict of interest, please contact us directly on our discord https://modmyclassic.com/discord
```

## How to use

### Project ERIS (PlayStation Classic)
1. Download the .mod package from [the Project ERIS mods page](https://modmyclassic.com/project-eris-mods/)
2. Copy the .mod package to your USB to: `USB:/project_eris/mods/`
3. Plug in and boot up the PlayStation Classic. Project ERIS will automatically install.
4. Select your custom theme from the options menu at the boot menu

**Note: It replaces the original bootmusic, but I included
a backup of the original in \project_eris\etc\project_eris\SND\backup of original
just incase you don't like it.**

## Credits

**Music Credits:**

Scandroid - "Neo-Tokyo"

**Wallpaper Credits:**

"Vaporwave" by TerribleMystery

**Based on themes by:**

"Carbon" by Eric Hettervik, "BaseVid" by Matt Kennedy, "Retrowave" by Nick Earl

## Preview

![preview](https://cdn.discordapp.com/attachments/565502413327302666/692290071524671498/PhotoCollage_20200320_160724339.jpg)